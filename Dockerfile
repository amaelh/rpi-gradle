FROM balenalib/raspberrypi3-openjdk:11-latest

# Start Cross build
RUN [ "cross-build-start" ]

RUN apt-get update && \
    apt-get -y install wget unzip ca-certificates && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

ENV GRADLE_VERSION 6.3

RUN wget "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" && \
    unzip "gradle-${GRADLE_VERSION}-bin.zip" -d /usr/src/ && \
    rm "gradle-${GRADLE_VERSION}-bin.zip" && \
    ln -s "/usr/src/gradle-${GRADLE_VERSION}/bin/gradle" /usr/bin/gradle

COPY . /usr/src/app

# End Cross build
RUN [ "cross-build-end" ]  

CMD /usr/src/app/run.sh
