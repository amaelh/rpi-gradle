DOCKER_IMAGE_VERSION=6.3
DOCKER_IMAGE_NAME=amaelh/rpi-gradle
DOCKER_IMAGE_TAGNAME=$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_VERSION)

default: push

build:
		docker build -t $(DOCKER_IMAGE_TAGNAME) .
		docker tag $(DOCKER_IMAGE_TAGNAME) $(DOCKER_IMAGE_NAME):latest

push: build
		docker push $(DOCKER_IMAGE_NAME)

